// right choice
var correctNum = 0;

// wrong choice
var wrongNum = 0;

var sceneNum = 0;

// Whether saving the current scene is successful
var succeed = false;

$(function() {
	// scene
	var sceneArr = [{
			dom: $('#scene1'),
			titleImg: './img/Task1.png',
			background: './img/background1.png',
			choiceNum: 2
		},
		{
			dom: $('#scene2'),
			titleImg: './img/Task2.png',
			background: './img/background2.png',
			choiceNum: 1

		}
	];

	// countdown
	var timer = null;

	function countdown() {
		var time = 300;
		var timeEle = $('#countdown');
		timer = setInterval(function() {
			time--;
			timeEle.text(time + ' s');
			// If time to 0 then clear the countdown
			if (time == 0) {
				wrongNum += 10;
				judge();
			}
		}, 1000);
	}

	$('#startButton').on('click', function() {
		$('#countdown').text(300 + ' s');
		$('#start').fadeOut();
		countdown();
	});

	// new game
	$('#endButton').on('click', function() {
		//Determine whether the current scene is successful
		if (sceneNum == 0 && succeed) {
			setTimeout(function() {
				sceneArr[sceneNum].dom.hide();
				sceneNum++;
				$('#start .title').prop('src', sceneArr[sceneNum].titleImg);
				$('#background').css('background', 'url(' + sceneArr[sceneNum].background + ') no-repeat center/cover');
				sceneArr[sceneNum].dom.show();
				$('#end').hide();
				$('#start').show();
			}, 400);
		} else {
			setTimeout(function() {
				if(sceneNum == 0 || succeed) {
					location.reload();
				} else {
					$('#end').hide();
					$('#start').show();
				}
				
			}, 400);
		}


	});

	// Sound effect
	$('#endButton,#startButton,.mdui-btn,.countdown-box,#quit,.btn1').on('click', function() {
		// Play the sound
		$('#audio').prop('src', './mp3/Click2.mp3');
	});
	// Sound effect2
	$('.btn').on('click', function() {
		// Play the sound
		$('#audio').prop('src', './mp3/Click1.mp3');
	});



	// Scene1 start
	var dialog5 = new mdui.Dialog('#dialog5', {
		modal: true
	});


	$('#dialog5')[0].addEventListener('closed.mdui.dialog', function() {
		judge();
	});


	var dialog4 = new mdui.Dialog('#dialog4', {
		modal: true
	});

	$('#dialog4')[0].addEventListener('closed.mdui.dialog', function() {
		judge();
	});
	$('#dialog4-btn1,#dialog5-btn2').on('click', function() {
		correctNum++;
	});
	$('#dialog4-btn2,#dialog4-btn3,#dialog5-btn1').on('click', function() {
		wrongNum++;
	});
	$('#game')[0].addEventListener('confirm.mdui.dialog', function() {
		correctNum++;
	});
	$('#game')[0].addEventListener('cancel.mdui.dialog', function() {
		dialog5.open();
	});
	$('#game')[0].addEventListener('closed.mdui.dialog', function() {
		$('#computer').hide();
		judge();
	});


	$('#sleep')[0].addEventListener('confirm.mdui.dialog', function() {

		$('#tip1').fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				dialog4.open();
			});
		});
	});
	$('#sleep')[0].addEventListener('cancel.mdui.dialog', function() {
		wrongNum++;
	});
	$('#sleep')[0].addEventListener('closed.mdui.dialog', function() {
		$('#bad').hide();
		judge();
	});
	// Scene1 end
	// Scene2 start
	var dialog6 = new mdui.Dialog('#scene2-dialog6', {
		modal: true
	});
	var dialog7 = new mdui.Dialog('#scene2-dialog7', {
		modal: true
	});
	var dialog8 = new mdui.Dialog('#scene2-dialog8', {
		modal: true
	});
	var dialog9 = new mdui.Dialog('#scene2-dialog9', {
		modal: true
	});

	var tip2Img = $('#scene2-tip2');
	var tip2 = $('#tip2');
	$('#man').on('click', function() {
		dialog6.open();
	});

	$('#scene2-dialog6-btn2').on('click', function() {
		tip2Img.prop('src', './img/iknowher.png')
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				dialog7.open();
			});
		});
	});

	$('#scene2-dialog7-btn1,#scene2-dialog7-btn2').on('click', function() {
		tip2Img.prop('src', './img/idontknow.png');
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				wrongNum++;
				judge();
			});
		});
	});

	$('#scene2-dialog6-btn1').on('click', function() {
		tip2Img.prop('src', './img/whatsthematter.png')
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				dialog8.open();
			});
		});
	});

	$('#scene2-dialog8-btn1').on('click', function() {
		tip2Img.prop('src', './img/whatthehell--.png');
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				wrongNum++;
				judge();
			});
		});
	});

	$('#scene2-dialog8-btn2').on('click', function() {
		tip2Img.prop('src', './img/sorryidontknow.png');
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
	
				wrongNum++;
				judge();
			});
		});
	});

	$('#scene2-dialog8-btn3').on('click', function() {
		tip2Img.prop('src', './img/hahathaks.png')
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				dialog9.open();
			});
		});
	});
	$('#scene2-dialog9-btn1').on('click', function() {
		tip2Img.prop('src', './img/ofcourse.png');
		tip2.fadeIn(2000, function() {
			$(this).fadeOut(3000, function() {
				correctNum++;
				judge();
			});
		});
	});

	// Scene2 end

	var dialog3 = $('#dialog3');
	$('#quit').on('click', function() {
		dialog3.slideDown();
	});

	$('#reset').on('click', function() {
		correctNum = 0;
		wrongNum = 0;
		clearInterval(timer);
		countdown();
		if (sceneNum == 0) {
			$('#computer').show();
			$('#bad').show();
		}
		dialog3.slideUp();
	});
	$('#view-task').on('click', function() {
		setTimeout(function() {
			location.reload();
		}, 400);
	});

	function judge() {
		if (correctNum + wrongNum < sceneArr[sceneNum].choiceNum) return;
		var result = $('#result');
		var audio = $('#audio');
		if (wrongNum > 0) {
			// Defeat
			succeed = false;
			if(!sceneNum){
				result.prop('src', './img/defeat.png');
			}else{
				result.prop('src', './img/defeat-1.png');
			}
			// Play the sound
			audio.prop('src', './mp3/Game Over.mp3');
		} else {
			// Success
			succeed = true;
			if(!sceneNum){
				result.prop('src', './img/success.png');
			}else{
				result.prop('src', './img/Success-1.png');
			}
			// Play the sound
			audio.prop('src', './mp3/Success.mp3');
		}
		// Clear the countdown
		clearInterval(timer);

		correctNum = 0;
		wrongNum = 0;

		$('#end').css({
			display: 'flex',
			opacity: '0'
		});
		$("#end").animate({
			opacity: 1
		}, 1000);

	}
});
