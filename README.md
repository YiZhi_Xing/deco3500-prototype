## Guide
This file will provide a guide to the contents of the repository.

## Poster
The poster is in the poster folder, showing the definition and concept of the problem space, 
and how the prototype works.

## Prototype
The prototype is based on web design, so please download the entire prototype folder and run the index.html file.

## Documentataion Link
[Home](https://gitlab.com/YiZhi_Xing/deco3500-prototype/-/wikis/home)

